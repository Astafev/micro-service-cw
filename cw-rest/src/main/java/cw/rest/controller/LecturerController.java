package cw.rest.controller;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import cw.api.services.LecturerService;
import cw.api.services.NewsService;
import cw.api.services.UserService;
import cw.dto.entities.Course;
import cw.dto.entities.Mark;
import cw.dto.entities.News;
import cw.dto.entities.Person;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.ws.rs.core.Application;
import java.io.File;
import java.util.HashMap;
import java.util.List;

@Component(
        immediate = true,
        property = {
                "liferay.auth.verifier=false",
                "liferay.oauth2=false",
                "jaxrs.application=true",
                JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/lecturer",
                JaxrsWhiteboardConstants.JAX_RS_NAME + "=Lecturer"
        },
        service = Application.class
)
public class LecturerController extends Application{

    private final UserService userService;

    private final LecturerService lectService;

    private final NewsService newsService;

    @Autowired
    public LecturerController(UserService userService, LecturerService lectService, NewsService newsService) {
        this.userService = userService;
        this.lectService = lectService;
        this.newsService = newsService;
    }

    private static final Log _log = LogFactoryUtil.getLog(
            AdminController.class);

    @Activate
    public void activate() {
        if (_log.isInfoEnabled()) {
            _log.info("Lecturer Rest module activated");
        }
    }

    @GetMapping("/coursemarks")
    public String studentMarks(Model model) {

        String username = userService.getCurrentUsername();
        Person lecturer = userService.findLecturerUser(username).get(0);
        Long lecturerId = lecturer.getId();
        List<Course> course = lectService.getLecturerCourses(lecturerId);
        List<Mark> marks = null;
        String c_name = course.get(0).getName();
        try {
            marks = lectService.getCourseMarks(course.get(0).getId());
        } catch (Exception ex) {
        }

        model.addAttribute("coursename", c_name);
        model.addAttribute("courseMarks", marks);
        return "coursemarks";
    }

    @GetMapping("/course_news")
    public String getCourseNews(Model model) {

        String username = userService.getCurrentUsername();
        model.addAttribute("news", newsService.getNewsByLecturerUsername(username));
        return "coursepage";
    }

    @GetMapping("/course_news/{newsId}")
    public String getNewsArticle(@PathVariable("newsId") Long newsId,
                                 Model model) {
        model.addAttribute("article", newsService.getById(newsId));
        return "news";
    }

    @PostMapping("/course_news/article={newsId}")
    public String updateArticle(@PathVariable("newsId") Long newsId,
                                @RequestParam(required = true, defaultValue = "" ) String newsTitle,
                                @RequestParam(required = true, defaultValue = "" ) String newsText,
                                @RequestParam(required = true, defaultValue = "" ) File newsFile,
                                @RequestParam(required = true, defaultValue = "" ) String action){

        if(action.equals("delete"))
            newsService.deleteNews(newsId);
        else {
            News news = new News();
            news.setId(newsId);
            news.setText(newsText);
            news.setTitle(newsTitle);
            news.setAttachment(newsFile.getPath());
            newsService.updateNews(news);

        }
        return "redirect:coursepage";
    }
}
