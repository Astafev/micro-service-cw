package cw.rest.controller;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import cw.api.services.PersonService;
import cw.api.services.UserService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.ws.rs.core.Application;

@Component(
        immediate = true,
        property = {
                "liferay.auth.verifier=false",
                "liferay.oauth2=false",
                "jaxrs.application=true",
                JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/admin",
                JaxrsWhiteboardConstants.JAX_RS_NAME + "=Admin"
        },
        service = Application.class
)
@Controller
public class AdminController extends Application{

        private final UserService userService;
        private final PersonService personService;

        @Autowired
        public AdminController(UserService userService, PersonService personService) {
                this.userService = userService;
                this.personService = personService;
        }

        private static final Log _log = LogFactoryUtil.getLog(
                AdminController.class);

        @Activate
        public void activate() {
                if (_log.isInfoEnabled()) {
                        _log.info("Admin Rest module activated");
                }
        }

        @GetMapping("/admin")
        public String userList(Model model) {
                model.addAttribute("allUsers", userService.allUsers());
                return "admin";
        }

        @PostMapping("/admin")
        public String  deleteUser(@RequestParam(required = true, defaultValue = "" ) Long userId,
                                  @RequestParam(required = true, defaultValue = "" ) String action) {
                if (action.equals("delete")){
                        userService.deleteUser(userId);
                        personService.deletePerson(userId);
                }
                return "redirect:/admin";
        }

        @GetMapping("/admin/gt/{userId}")
        public String  gtUser(@PathVariable("userId") Long userId, Model model) {
                model.addAttribute("allUsers", userService.userGetList(userId));
                return "admin";
        }
}
