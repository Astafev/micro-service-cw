package cw.rest.controller;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import cw.api.services.NewsService;
import cw.api.services.StudentService;
import cw.api.services.UserService;
import cw.dto.entities.Course;
import cw.dto.entities.Mark;
import cw.dto.entities.Person;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.ws.rs.core.Application;
import java.sql.SQLException;
import java.util.List;

@Component(
        immediate = true,
        property = {
                "liferay.auth.verifier=false",
                "liferay.oauth2=false",
                "jaxrs.application=true",
                JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/student",
                JaxrsWhiteboardConstants.JAX_RS_NAME + "=Student"
        },
        service = Application.class
)
public class StudentController extends Application{

    private final UserService userService;

    private final StudentService studService;

    private final NewsService newsService;

    @Autowired
    public StudentController(UserService userService, StudentService studService, NewsService newsService) {
        this.userService = userService;
        this.studService = studService;
        this.newsService = newsService;
    }

    private static final Log _log = LogFactoryUtil.getLog(
            AdminController.class);

    @Activate
    public void activate() {
        if (_log.isInfoEnabled()) {
            _log.info("Student Rest module activated");
        }
    }

    @GetMapping("/studmarks")
    public String studentMarks(Model model){

        Person student = userService.findStudentUser(userService.getCurrentUsername()).get(0);
        Long stud_id = student.getId();
        List<Mark> marks = null;
        try {
            marks = studService.getStudentMarks(stud_id);
        } catch (Exception ex) {
        }

        model.addAttribute("allMarks", marks);
        return "studmarks";
    }

    @GetMapping("/courseslist")
    public String coursesList(Model model) throws SQLException, ClassNotFoundException {

        Person student = userService.findStudentUser(userService.getCurrentUsername()).get(0);
        Long stud_id = student.getId();
        List<Course> courses = studService.getStudentCourses(stud_id);

        model.addAttribute("student",student);
        model.addAttribute("coursesList",courses);
        return "courseslist";
    }

    @GetMapping("/coursenews/{courseId}")
    public String courseNews(@PathVariable("courseId") Long courseId, Model model) {
        model.addAttribute("courseNews", newsService.getCourseNews(courseId));
        return "coursepage";
    }

    @GetMapping("/coursenews/{courseId}/{newsId}")
    public String courseNewsInfo(@PathVariable("courseId") Long courseId,
                                 @PathVariable("newsId") Long newsId,
                                 Model model) {
        model.addAttribute("course_article", newsService.getById(newsId, courseId));
        model.addAttribute("attachment", newsService.getFileByPath(
                newsService.getById(newsId, courseId).getAttachment()));
        return "news";
    }
}
