package cw.rest.controller;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import cw.api.services.UserService;
import cw.dto.entities.User;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import javax.ws.rs.core.Application;

@Component(
        immediate = true,
        property = {
                "liferay.auth.verifier=false",
                "liferay.oauth2=false",
                "jaxrs.application=true",
                JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/registration",
                JaxrsWhiteboardConstants.JAX_RS_NAME + "=Registration"
        },
        service = Application.class
)
public class RegistrationController extends Application{

    private static final Log _log = LogFactoryUtil.getLog(
            AdminController.class);

    @Activate
    public void activate() {
        if (_log.isInfoEnabled()) {
            _log.info("Registration Rest module activated");
        }
    }

    @Autowired
    private UserService userService;

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new User());

        return "registration";
    }

    @PostMapping("/registration")
    public String addUser(@ModelAttribute("userForm") @Valid User userForm, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            return "registration";
        }
        if (!userForm.getPassword().equals(userForm.getPasswordConfirm())){
            model.addAttribute("passwordError", "Пароли не совпадают");
            return "registration";
        }
        if (!userService.saveUser(userForm)){
            model.addAttribute("usernameError", "Пользователь с таким именем уже существует");
            return "registration";
        }

        return "redirect:/";
    }
}
