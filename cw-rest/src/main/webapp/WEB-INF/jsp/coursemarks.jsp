<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Log in with your account</title>
  <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/style.css">


  <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
  <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>

<body>
<div class="container">
    <c:set var="username" scope="request" value="${pageContext.request.userPrincipal.name}"/>
    <h4>Курс: <c:out value="${coursename}"/></h4>
    <h1></h1>
    <table>
      <thead>
      <th>Студент</th>
      <th>Оценка</th>
      </thead>
      <c:forEach items = "${courseMarks}" var = "row">
        <tr>
          <td>
            <c:out value="${row.key}"/></td> <td><c:out value="${row.value}"/>
        </td>
        </tr>
      </c:forEach>
    </table>
  <a href="/">Главная</a>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>