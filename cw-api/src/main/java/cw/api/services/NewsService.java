package cw.api.services;

import cw.dto.entities.News;
import cw.dto.repository.NewsRepository;
import cw.dto.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.File;
import java.net.URL;
import java.util.List;

@Service
public class NewsService {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    NewsRepository newsRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    public News getById(Long id) {
        return newsRepository.getById(id);
    }

    public News getById(Long news_id, Long course_id){
        return em.createNamedQuery("SELECT n FROM News n\n" +
                "WHERE n.course_id = :course_id AND n.id = :news_id", News.class)
                .setParameter("course_id", course_id)
                .setParameter("news_id", news_id)
                .getSingleResult();
    }

    public Integer updateNews(News news) {
        return em.createNamedQuery("update t_news tn\n" +
                "set title = :title\n" +
                ", text = :text\n" +
                ", attachment_path = :attachment\n" +
                ", author_id = :author_id\n" +
                " where tn.id = :id", Integer.class)
                .setParameter("title", news.getTitle())
                .setParameter("text", news.getText())
                .setParameter("attachment", news.getAttachment())
                .setParameter("author_id", userRepository.findByUsername(userService.getCurrentUsername()))
                .setParameter("id", news.getId())
                .getSingleResult();
    }

    public boolean saveNews(News news) {
        News newsFromDb = newsRepository.getById(news.getId());

        if(newsFromDb!=null)
            return false;

        newsRepository.save(news);
        return true;
    }

    public boolean deleteNews(Long id) {
        if(newsRepository.getById(id) != null){
            newsRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public List<News> getCourseNews(Long course_id) {
        return em.createNamedQuery("SELECT n FROM News n\n" +
                "where n.course_id = :course_id", News.class)
                .setParameter("course_id", course_id)
                .getResultList();
    }

    public List<News> getNewsByLecturerUsername(String username) {
        return em.createNamedQuery("SELECT n FROM News n\n" +
                "INNER JOIN Person p\n" +
                "INNER JOIN User u\n " +
                "where u.username = :username", News.class)
                .setParameter("username", username)
                .getResultList();
    }

    public File getFileByPath(String path){

        File file = null;

        URL url = getClass().getResource(path);
        try{
            file = new File(url.getPath());
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
        return file;
    }
}