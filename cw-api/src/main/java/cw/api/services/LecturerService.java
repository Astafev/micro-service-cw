package cw.api.services;

import cw.dto.entities.Course;
import cw.dto.entities.Mark;
import cw.dto.entities.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class LecturerService {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    PersonService personService;

    @Autowired
    UserService userService;

    public Person getLecturer(Person person) {
        return personService.findById(person.getId());
    }

    public Integer updateLecturer(Person person) {
        return personService.updatePerson(person);
    }

    public boolean deleteLecturer(Person person){
        return personService.deletePerson(person.getId());
    }

    public List<Course> getLecturerCourses(Long id){
        return em.createNativeQuery("select * from t_course c \n" +
                "inner join t_person tl on c.id = tl.course_id\n" +
                "where tl.id = :lecturer_id", Course.class)
                .setParameter("lecturer_id", id).getResultList();
    }

    public List<Mark> getCourseMarks(Long course_id){
        return em.createNamedQuery("SELECT m FROM Mark m\n" +
                "WHERE m.course_id = :id", Mark.class)
                .setParameter("id", course_id)
                .getResultList();
    }
}
