package cw.api.services;

import cw.dto.entities.Course;
import cw.dto.entities.Mark;
import cw.dto.entities.Person;
import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class StudentService {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    PersonService personService;

    @Autowired
    UserService userService;

    public Person getStudent(Long id){
        return personService.findById(id);
    }

    public boolean deleteStudent(Person person) {
        return personService.deletePerson(person.getId());
    }

    public List<Mark> getStudentMarks(Long id) {
        return em.createNamedQuery("SELECT m FROM Mark m\n" +
                "WHERE m.person_id = :id", Mark.class)
                .setParameter("id", id)
                .getResultList();
    }

    public List<Course> getStudentCourses(Long id){
        return em.createNamedQuery("SELECT c FROM Course c\n" +
                "JOIN Mark m\n" +
                "JOIN Person p\n" +
                "WHERE p.id = :id", Course.class)
                .setParameter("id", id)
                .getResultList();
    }
}