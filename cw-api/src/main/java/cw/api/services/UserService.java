package cw.api.services;

import cw.dto.entities.Person;
import cw.dto.entities.Role;
import cw.dto.entities.User;
import cw.dto.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class UserService implements UserDetailsService {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        if(user == null)
            throw new UsernameNotFoundException("User with name:" + username + " not found");

        return user;
    }

    public List<User> allUsers(){
        return userRepository.findAll();
    }

    public boolean saveUser(User user){
        User userFromDb = userRepository.findByUsername(user.getUsername());

        if(userFromDb!=null)
            return false;

        user.setRole(new Role("STUDENT"));
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);

        return true;
    }

    public boolean deleteUser(Long id){
        if(userRepository.findById(id).isPresent()){
            userRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public List<User> userGetList(Long idMin) {
        return em.createQuery("SELECT u FROM User u WHERE u.id > :paramId", User.class)
                .setParameter("paramId", idMin).getResultList();
    }

    public List<Person> findStudentUser(String username) {

        return em.createNativeQuery("select * from t_person tp s\n" +
                "inner join t_user tu on tp.user_id = tu.id\n" +
                "where tu.username = :username", Person.class)
                .setParameter("username", username).getResultList();
    }

    public List<Person> findLecturerUser(String username){
        return em.createNativeQuery("select * from t_person tp s\n" +
                "inner join t_user tu on tp.user_id = tu.id\n" +
                "where tu.username = :username", Person.class)
                .setParameter("username", username).getResultList();
    }

    public String getCurrentUsername() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getName();
    }
}