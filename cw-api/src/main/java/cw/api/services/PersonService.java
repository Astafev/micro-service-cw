package cw.api.services;

import cw.dto.entities.Person;
import cw.dto.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class PersonService {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    UserService userService;

    @Autowired
    PersonRepository personRepository;

    public Person findById(Long id){
        return personRepository.getById(id);
    }

    public List<Person> getByUsername(String username){
        return em.createNamedQuery("select * from t_person tp\n" +
                "inner join t_user tu on tp.user_id = tu.id\n" +
                "where tu.username = :username", Person.class)
                .setParameter("username", username).getResultList();
    }

    public boolean savePerson(Person person) {
        Person personFromDb = personRepository.getById(person.getId());

        if(personFromDb!=null)
            return false;

        personRepository.save(person);
        return true;
    }

    public Integer updatePerson(Person person) {
        return em.createNamedQuery("update t_person tp\n" +
                "set name = :name\n" +
                ", surname = :surname\n" +
                ", patronymic = :patronymic\n" +
                " where tp.id = :id", Integer.class)
                .setParameter("name", person.getName())
                .setParameter("surname", person.getSurname())
                .setParameter("patronymic", person.getPatronymic())
                .setParameter("id", person.getId()).getSingleResult();
    }

    public boolean deletePerson(Long id) {
        if(personRepository.getById(id) != null){
            personRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public List<Person> getPositionedPerson(Person person) {
        return em.createNamedQuery("select p.name, p.surname, p.patronymic, pos.title\n" +
                "from t_person tp, t_position tpos\n" +
                "inner join pos on pos.id = p.position_id\n"+
                "where p.id = :id", Person.class)
                .setParameter("id", person.getId()).getResultList();
    }

    public List<Person> allPersons(){
        return personRepository.findAll();
    }

    public List<Person> allPersonsPositioned(){
        return em.createNamedQuery("select p.name, p.surname, p.patronymic, pos.title\n" +
                "from t_person tp, t_position tpos\n" +
                "inner join pos on pos.id = p.position_id\n", Person.class)
                .getResultList();
    }
}