package cw.dto.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Entity
@Table(name = "t_role")
@Getter
@Setter
@NoArgsConstructor

public class Role implements GrantedAuthority {

    @Id
    private Long id;

    @Column(name = "name")
    private String name;

    @Transient
    @OneToOne(mappedBy = "roles")
    private User user;

    @Override
    public String getAuthority() { return getName(); }

    public Role(String name){
        this.name = name;
    }
}
