package cw.dto.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "t_course")
@Getter
@Setter
@NoArgsConstructor
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "course_news")
    private Set<News> news;

    @OneToMany(mappedBy = "course_mark")
    private Set<Mark> marks;

    @OneToMany(mappedBy = "course_person")
    private Set<CourseToLecturer> ctl;
}
